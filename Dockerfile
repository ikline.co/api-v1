FROM node:12.18.3

RUN apt-get update && apt-get install htop

COPY package.json yarn.lock /var/www/

WORKDIR /var/www/

RUN yarn install

COPY . /var/www/

EXPOSE 4000

CMD ["yarn", "debug"]