const express = require("express");
const path = require("path");
const morgan = require("morgan");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const i18n = require("i18n");

const errorsList = require("./shared/errorsList");
const router = require("./router");
const logger = require("./shared/logger");
const { HttpError } = require("./errors");

const API_VERSION = `v${process.env.API_VERSION.split(".")[0]}`;

/**
 * Make every first letter, of every word in the msg, uppercase.
 * @param {String} msg The message to transform.
 */
const transformMessage = (msg) =>
  msg
    .split(" ")
    .map((word) => word[0].toUpperCase() + word.substring(1))
    .join(" ");

/**
 * If any route matched the url request, creates a new 404 route not found error.
 * @param {Function} req
 * @param {Function} res
 * @param {Function} next
 */
const handleRouteNotFound = (req, res, next) => {
  next(new HttpError(404, "Route Not Found"));
};

/**
 * Handles any error passed to a next function.
 * @param {Error} err
 * @param {Function} req
 * @param {Function} res
 * @param {Function} next
 */
const handleAllErrors = (err, req, res, next) => {
  // Let's merge the default error with the throwned one. Additionally, we'll deconstruct the result to ensure the order of the properties.
  const { status, message, description, ..._err } = {
    status: 500,
    ...errorsList[err.status || 500],
    ...err,
    message: transformMessage(
      err.message || errorsList[err.status || 500].message
    ),
  };

  logger.error(err);

  res.status(status).json({
    error: { status, message, description, ..._err },
  });
};

/**
 * This class is the entry point to this API.
 */
class App {
  constructor() {
    this.express = express();
    this.initMiddlewares();
    this.initRoutes();
    this.initErrorHandler();
  }

  initMiddlewares() {
    this.express.use(
      cors({
        origin: "*",
        optionsSuccessStatus: 200, // Some legacy browsers (IE11, various SmartTVs) choke on 204
      })
    );

    this.express.use(
      morgan("common", {
        stream: {
          write: function (message) {
            logger.info(message);
          },
        },
      })
    );

    this.express.use(express.json());

    this.express.use(express.urlencoded({ extended: false }));

    this.express.use(cookieParser());

    i18n.configure({
      locales: ["pt", "en"],
      defaultLocale: "pt",
      cookie: "language",
      queryParameter: "lang",
      directory: __dirname + "/i18n",
    });
    this.express.use(i18n.init);

    this.express.use(
      `/${API_VERSION}/static`,
      express.static(path.join(__dirname, "public"))
    );
  }

  initRoutes() {
    this.express.use(`/${API_VERSION}/`, router);

    this.express.use(handleRouteNotFound);
  }

  initErrorHandler() {
    this.express.use(handleAllErrors);
  }
}

module.exports = new App().express;
