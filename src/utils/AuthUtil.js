const { promisify } = require("util");
const jwt = require("jsonwebtoken");

const verifyJwtPromise = promisify(jwt.verify);

class AuthUtil {
  /**
   * Since it's a static, utilitary class, it can not be instantiated.
   */
  constructor() {
    throw new Error("This class can't be instantiated.");
  }

  static generateToken(payload) {
    return jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: "1h" });
  }

  static generateRefreshToken(payload) {
    return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
      expiresIn: "1d",
    });
  }

  static async verifyJWT(token) {
    return verifyJwtPromise(token, process.env.SECRET);
  }
}

module.exports = AuthUtil;
