const path = require("path");
const Umzug = require("umzug");

const logger = require("../shared/logger");
const db = require("../models");

class MigrationUtil {
  constructor() {
    throw new Error("This class cannot be instantiated");
  }

  static _unzug = new Umzug({
    migrations: {
      path: path.join(__dirname, "../database/migrations"),
      params: [db.sequelize.getQueryInterface(), db.Sequelize],
    },
    storage: "sequelize",
    storageOptions: {
      sequelize: db.sequelize,
    },
  });

  static async runAll() {
    logger.info("Trying to execute all migrations...");
    return MigrationUtil._unzug
      .up()
      .then((executedMigrations) => {
        logger.info("Execution completed successfully.");
        return executedMigrations.length
          ? executedMigrations.reduce(
              (log, migration) => `${log}\t- ${migration.file}\n`,
              "Executed Migrations:\n"
            )
          : "No migrations were executed, database schema was already up to date.";
      })
      .then((executionLog) => logger.info(executionLog))
      .catch((err) => {
        logger.error(
          "Execution failed since the following error occurred:",
          err
        );
        // if (process.env.NODE_ENV != "development") {
        // 	const env = process.env.NODE_ENV.toUpperCase();
        // 	const emailParams = {
        // 		subject: `Migration Error | ${env}`,
        // 		html: `<p>Execution failed since the following error occurred:</p></br><ul><li>Message - ${err.message}</li><li>Stack - ${err.stack}</li></ul>`,
        // 	};

        // 	return Promise.all(
        // 		FAILED_FEEDBACK_EMAILS.map((email) =>
        // 			EmailUtil.sendEmail({ to: email, ...emailParams })
        // 		)
        // 	);
        // }
      });
  }

  static async truncateAll() {
    logger.info("Trying to truncate all tables...");
    return db.sequelize.transaction((transaction) => db.sequelize.truncate({cascade: true, restartIdentity: true, transaction})).then(() => logger.info("All tables were truncated successfully.")).catch((err) => {
      logger.error(
        "Truncate execution failed since the following error occurred:",
        err
      );
    })
  }
}

module.exports = MigrationUtil;
