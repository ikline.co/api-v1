class PromiseUtil {
  /**
   * Prevent this class from being instantiated.
   */
  constructor() {
    throw new Error("This class can't be instantiated!");
  }

  static async timeoutPromise(milliseconds, promise) {
    const timeout = new Promise((resolve, reject) =>
      setTimeout(
        () => reject(`Limite da promise excedido (limite: ${milliseconds} ms)`),
        milliseconds
      )
    );

    return Promise.race([timeout, promise]);
  }

  static delay(milliseconds) {
    return async (data) =>
      new Promise((resolve, reject) =>
        setTimeout(() => resolve(data), milliseconds)
      );
  }

  static retry(retries, milliseconds, fn, ...params) {
    return fn(...params).catch((err) => {
      return PromiseUtil.delay(milliseconds)().then(() =>
        retries > 1
          ? PromiseUtil.retry(--retries, milliseconds, fn, ...params)
          : Promise.reject(err)
      );
    });
  }
}

module.exports = PromiseUtil;
