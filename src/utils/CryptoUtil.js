const bcrypt = require("bcrypt");

const SALT_ROUNDS = 10;

class CryptoUtil {
  /**
   * Since it's a static, utilitary class, it can not be instantiated.
   */
  constructor() {
    throw new Error("This class can't be instantiated.");
  }

  /**
   * Hashs a passowrd with bcrypt lib.
   * @param {String} plainPassword Plain password.
   * @returns {String } Hashed password.
   * @returns {Boolean } False if the plain password was not provided.
   */
  static hashPassword(plainPassword) {
    return plainPassword && bcrypt.hashSync(plainPassword, SALT_ROUNDS);
  }

  /**
   * Compares a password with a password hash and returns if it is valid.
   * @param {String} plainPassword Plain password to check.
   * @param {String} hashedPassword Hashed password to compare.
   * @returns {Boolean} If the password is valid.
   */
  static checkPassword(plainPassword, hashedPassword) {
    return bcrypt.compareSync(plainPassword, hashedPassword);
  }
}

module.exports = CryptoUtil;
