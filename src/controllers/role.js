const db = require("../models");
const Base = require("./Base");

class Role extends Base {
  constructor() {
    super(db.role);
    this.path = "/roles";
  }
}

module.exports = new Role();
