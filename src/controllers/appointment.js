const db = require("../models");
const Base = require("./Base");

class Appointment extends Base {
  constructor() {
    super(db.appointment);
    this.path = "/appointments";
  }

  list() {
    return {
      path: "/",
      verb: "get",
      auth: false,
      authz: {
        requiredPermissions: [],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        return super.list().method(req, res, next);
      },
    };
  }

  create() {
    return {
      path: "/",
      verb: "post",
      auth: false,
      authz: {
        requiredPermissions: [`${this.model.name}:create`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        return super.create().method(req, res, next);
      },
    };
  }

  update() {
    return {
      path: "/:id",
      verb: "patch",
      auth: false,
      authz: super.update().authz,
      validate: true,
      method: (req, res, next) => {
        return super.update().method(req, res, next);
      },
    };
  }

  delete() {
    return {
      path: "/:id",
      verb: "delete",
      auth: false,
      authz: {
        requiredPermissions: [`${this.model.name}:delete`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        return super.delete().method(req, res, next);
      },
    };
  }
}

module.exports = new Appointment();
