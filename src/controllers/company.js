const Base = require("./Base");
const db = require("../models");

class Company extends Base {
  constructor() {
    super(db.company);
    this.path = "/companies";
  }

  list(options = {}, extend = {}) {
    return {
      path: "/",
      verb: "get",
      auth: false,
      authz: {
        requiredPermissions: [],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        return super.list().method(req, res, next);
      },
    };
  }

  current() {
    return {
      path: "/current",
      verb: "get",
      auth: true,
      authz: { requiredPermissions: [], requiredRoles: [] },
      validate: false,
      method: (req, res, next) => {
        const id = req.user.currentCompanyId;
        return super.find({}, { id }).method(req, res, next);
      },
    };
  }

  update() {
    return {
      path: "/",
      verb: "patch",
      auth: true,
      authz: super.update().authz,
      validate: true,
      method: (req, res, next) => {
        const id = req.user.currentCompanyId;
        return super.update({}, { id }).method(req, res, next);
      },
    };
  }
}

module.exports = new Company();
