const db = require("../models")
const MigrationUtil = require("../utils/MigrationUtil")

describe("User Controller", () => {
	beforeAll(MigrationUtil.runAll);
	beforeEach(MigrationUtil.truncateAll);

	// it("Should Pass", () => expect(1).toBe(1));

    it("Should Create A Company", async () => {
        const iKline = await db.right.findAll();
        console.log(iKline)
        expect(iKline.length).toBe(0);
    });
});
