const db = require("../models");
const Base = require("./Base");

class User extends Base {
  constructor() {
    super(db.user);
    this.path = "/users";
  }

  list() {
    return {
      path: "/",
      verb: "get",
      auth: true,
      authz: super.list().authz,
      validate: true,
      method: (req, res, next) => {
        const companyId = req.user.currentCompanyId;
        return super
          .list({
            include: [
              ...this.includes.rightsAndRoles(companyId),
              {
                model: db.company,
                attributes: [],
                through: {
                  attributes: [],
                  where: { companyId },
                  required: true,
                },
                required: true,
              },
            ],
          })
          .method(req, res, next);
      },
    };
  }

  // find() {
  //   return (req, res, next) => {
  //     const companyId = req.user.currentCompanyId;
  //     return super
  //       .find({
  //         include: rightsAndRoles(companyId),
  //       })
  //       .method(req, res, next);
  //   };
  // }

  // signUp() {
  //   return (req, res, next) => {
  //     const { rights, roles, ...user } = req.validated.body;

  //     return db.sequelize
  //       .transaction(async (transaction) => {
  //         const [targetCompany, targetUser, ...validRoles] = await Promise.all([
  //           companyDao
  //             .findById(user.companyId, { transaction })
  //             .then(
  //               this._resourceNotFound(
  //                 `Company of id ${user.companyId} not found.`
  //               )
  //             ),
  //           this.model.findOne({
  //             where: { email: user.email },
  //             transaction,
  //           }),
  //           ...roles.map(({ name }) =>
  //             db.role
  //               .findOne({
  //                 where: { name, companyId: user.companyId },
  //                 transaction,
  //               })
  //               .then(this._resourceNotFound(`Role ${name} not found.`))
  //           ),
  //         ]);

  //         if (targetUser && (await targetUser.hasCompanies([targetCompany]))) {
  //           return next(
  //             new this._errors.StatusError(
  //               422,
  //               `User already exists in ${targetCompany.name}.`
  //             )
  //           );
  //         }
  //         const userToRegister =
  //           targetUser || (await this.model.create(user, { transaction }));

  //         await Promise.all([
  //           userToRegister.addCompanies(targetCompany, {
  //             transaction,
  //           }),
  //           userToRegister.setCompany(targetCompany, {
  //             transaction,
  //           }),
  //           userToRegister.addRoles(validRoles, {
  //             transaction,
  //           }),
  //         ]);
  //         return userToRegister;
  //       })
  //       .then((registeredUser) =>
  //         res.status(201).send({
  //           meta: { success: true },
  //           data: {
  //             user: registeredUser.toDto(),
  //             token: registeredUser.generateToken(),
  //             refreshToken: registeredUser.refreshToken,
  //           },
  //         })
  //       )
  //       .catch(next);
  //   };
  // }

  // login() {
  //   return (req, res, next) => {
  //     const { email, password } = req.validated.body;

  //     return this.model
  //       .unscoped()
  //       .findOne({ where: { email } })
  //       .then(this._resourceNotFound(`User of email ${email} not found.`))
  //       .then((user) => {
  //         if (user.checkPassword(password)) {
  //           return user
  //             .set("refreshToken", user.generateRefreshToken())
  //             .save()
  //             .then((savedUser) =>
  //               res.status(201).send({
  //                 meta: { success: true },
  //                 data: {
  //                   user: savedUser.toDto(),
  //                   token: savedUser.generateToken(),
  //                   refreshToken: savedUser.refreshToken,
  //                 },
  //               })
  //             )
  //             .catch(next);
  //         } else {
  //           return res.sendStatus(401);
  //         }
  //       })
  //       .catch(next);
  //   };
  // }

  // changeCurrentCompany() {
  //   return (req, res, next) => {
  //     const { companyId: targetCompanyId } = req.validated.body;
  //     const { id, companyId } = req.user;

  //     if (targetCompanyId == companyId) {
  //       return next(
  //         new this._errors.StatusError(
  //           422,
  //           `User is already logged in company of id ${companyId}.`
  //         )
  //       );
  //     }

  //     return db.sequelize
  //       .transaction(async (transaction) => {
  //         const [targetUser, targetCompany] = await Promise.all([
  //           this.model.findOne({
  //             where: { id },
  //             transaction,
  //           }),
  //           companyDao.findById(targetCompanyId, { transaction }),
  //         ]);

  //         const hasAccess = await targetUser.hasCompanies([targetCompany], {
  //           transaction,
  //         });
  //         if (!hasAccess) {
  //           return next(
  //             new this._errors.StatusError(
  //               403,
  //               "User does not have access to this company."
  //             )
  //           );
  //         }
  //         await targetUser.setCompany(targetCompany, { transaction });
  //         console.log("Company Id: ", targetUser);
  //         const updatedUser = await targetUser
  //           .set("refreshToken", targetUser.generateRefreshToken(), {
  //             transaction,
  //           })
  //           .save({ transaction });

  //         return res.status(201).send({
  //           meta: { success: true },
  //           data: {
  //             user: updatedUser.toDto(),
  //             token: updatedUser.generateToken(),
  //             refreshToken: updatedUser.refreshToken,
  //           },
  //         });
  //       })
  //       .catch(next);
  //   };
  // }

  // me() {
  //   return (req, res, next) => {
  //     const id = req.user.id;
  //     return this.model
  //       .findOne({
  //         include: rightsAndRoles(req.user.currentCompanyId),
  //       })
  //       .then(this._resourceNotFound(`User of id ${id} not found.`))
  //       .then((user) => res.send({ user: user.toUserWithRightsAndRolesDto() })) // tá fora do pedrão a reposta, faltando o meta e data.
  //       .catch(next);
  //   };
  // }

  // refreshToken() {
  //   return (req, res, next) => {
  //     const { refreshToken } = req.validated.body;
  //     return this.model
  //       .findOne({ where: { refreshToken } })
  //       .then(this._resourceNotFound(`User not found`))
  //       .then((user) =>
  //         user.set("refreshToken", user.generateRefreshToken()).save()
  //       )
  //       .then((updatedUser) => {
  //         res.status(201).send({
  //           meta: { success: true },
  //           data: {
  //             user: updatedUser.toDto(),
  //             token: updatedUser.generateToken(),
  //             refreshToken: updatedUser.refreshToken,
  //           },
  //         });
  //       })
  //       .catch(next);
  //   };
  // }

  // forgot() {
  //   return (req, res, next) => {
  //     const { email } = req.validated.body;

  //     return this.model
  //       .unscoped()
  //       .findOne({ where: { email } })
  //       .then(this._resourceNotFound(`User of email ${email} not found.`))
  //       .then((_user) =>
  //         _user.set("refreshToken", _user.generateRefreshToken()).save()
  //       )
  //       .then(async (updatedUser) => {
  //         const link = `${req.origin}/login/alterar-senha/${updatedUser.refreshToken}`;
  //         const company = await companyDao.getCompanyWithEmailConfigById(
  //           updatedUser.companyId
  //         );
  //         return EmailUtil.sendEmailTemplate("forgot-password", email, {
  //           company,
  //           link,
  //         }).then(() => updatedUser);
  //       })
  //       .then((updatedUser) => {
  //         res.status(201).send({
  //           meta: { success: true },
  //           data: {
  //             user: updatedUser.toDto(),
  //             token: updatedUser.generateToken(),
  //             refreshToken: updatedUser.refreshToken,
  //           },
  //         });
  //       })
  //       .catch(next);
  //   };
  // }

  // reset() {
  //   return (req, res, next) => {
  //     const { password, refreshToken } = req.validated.body;

  //     this.model
  //       .update(
  //         { password },
  //         {
  //           where: { refreshToken },
  //           returning: true,
  //         }
  //       )
  //       .then(this._resourceNotFound(`User not found`))
  //       .then((data) => {
  //         /**
  //          * Sequelize update return an array [x, [...y]],
  //          * which x is the number of affected rows and y`s,
  //          * the actual returned data
  //          */
  //         const updatedUser = data[1][0];
  //         res.status(200).send({
  //           meta: { success: true },
  //           data: updatedUser.toDto(),
  //         });
  //       })
  //       .catch(next);
  //   };
  // }

  // update() {
  //   return (req, res, next) => {
  //     const id = req.validated.params.id;
  //     const { rights, roles, ...user } = req.validated.body;
  //     return db.sequelize
  //       .transaction(async (transaction) => {
  //         // Sequelize update return an array [x, [...y]], which x is the number of affected rows and y`s, the actual returned data
  //         const updatedUser = (
  //           await this.model
  //             .update(user, {
  //               where: { id },
  //               returning: true,
  //               transaction,
  //             })
  //             .then(this._resourceNotFound(`User of id ${id} not found.`))
  //         )[1][0];
  //         if (roles) {
  //           const validRoles = await Promise.all(
  //             roles.map(({ name }) =>
  //               db.role
  //                 .findOne({
  //                   where: { name },
  //                   transaction,
  //                 })
  //                 .then(this._resourceNotFound(`Role ${name} not found.`))
  //             )
  //           );

  //           await updatedUser.setRoles(validRoles, {
  //             transaction,
  //           });
  //         }

  //         return userDao.getUserWithRightsAndRolesById(id, { transaction });
  //       })
  //       .then((updatedUser) =>
  //         res.status(200).send({
  //           meta: {
  //             success: true,
  //           },
  //           data: updatedUser,
  //         })
  //       )
  //       .catch(next);
  //   };
  // }
}

module.exports = new User();
