const logger = require("../shared/logger");
const errors = require("../errors");
const includes = require("../shared/includes");

class Base {
  constructor(model) {
    this.logger = logger;
    this.model = model;
    this.includes = includes;
    this.errors = errors;
  }

  list(options = {}, extend = {}) {
    return {
      path: "/",
      verb: "get",
      auth: true,
      authz: {
        requiredPermissions: [`${this.model.name}:list`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        const page = req.validated.query.page || 0;
        const size = req.validated.query.size || 30;

        return this.model
          .findAndCountAll({ ...options, limit: size, offset: page * size })
          .then(({ count: totalCount, rows }) => {
            res.status(200).send({
              totalCount,
              data: rows.map((row) => (row.toDto ? row.toDto() : row)),
            });
          })
          .catch(next);
      },
    };
  }

  find(options = {}, extend = { id: undefined }) {
    return {
      path: "/:id",
      verb: "get",
      auth: true,
      authz: {
        requiredPermissions: [`${this.model.name}:find`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        const id = extend.id || req.validated.params.id;
        return this.model
          .findByPk(id, options)
          .then(
            this._resourceNotFound(
              `${this.model.name}, of id ${id}, not found.`
            )
          )
          .then((instance) => {
            res.status(200).send({
              data: instance.toDto ? instance.toDto() : instance,
            });
          })
          .catch(next);
      },
    };
  }

  delete(options = {}) {
    return {
      path: "/:id",
      verb: "delete",
      auth: true,
      authz: {
        requiredPermissions: [`${this.model.name}:delete`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        return this.model
          .destroy({
            ...options,
            where: {
              id: req.validated.params.id,
            },
          })
          .then(
            this._resourceNotFound(
              `${this.model.name}, of id ${req.validated.params.id}, not found.`
            )
          )
          .then((isDeleted) => {
            res.status(200).send({
              data: !!isDeleted,
            });
          })
          .catch(next);
      },
    };
  }

  update(options = {}, extend = { id: undefined }) {
    return {
      path: "/:id",
      verb: "patch",
      auth: true,
      authz: {
        requiredPermissions: [`${this.model.name}:update`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        const toUpdate = req.validated.body;
        const id = extend.id || req.validated.params.id;
        return this.model
          .update(toUpdate, {
            ...options,
            where: { id },
            returning: true,
          })
          .then(
            this._resourceNotFound(
              `${this.model.name}, of id ${id}, not found.`
            )
          )
          .then((data) => {
            // Sequelize update return an array [x, [...y]], which x is the number of affected rows and y`s, the actual returned data
            const updatedInstance = data[1][0];
            res.status(200).send({
              data: updatedInstance.toDto
                ? updatedInstance.toDto()
                : updatedInstance,
            });
          })
          .catch(next);
      },
    };
  }

  create(options = {}, extend = { body: {} }) {
    return {
      path: "/",
      verb: "post",
      auth: true,
      authz: {
        requiredPermissions: [`${this.model.name}:create`],
        requiredRoles: [],
      },
      validate: true,
      method: (req, res, next) => {
        const toCreate = { ...req.validated.body, ...extend.body };
        return this.model
          .create(toCreate, options)
          .then((createdInstance) =>
            res.status(201).send({
              data: createdInstance.toDto
                ? createdInstance.toDto()
                : createdInstance,
            })
          )
          .catch(next);
      },
    };
  }

  /**
   * Returns a callback that throws a 404 error if a resource is not found. Ideal to be used after sequelize methods that
   * returns some data object, like findOne, or that returns an array with the number os rows that were updated, like in update.
   * @param {String} message The message of the error that will be throwed, defaults to "Resource Not Found".
   */
  _resourceNotFound(message) {
    return (data) => {
      if (!data || data[0] <= 0)
        throw new this._errors.HttpError(
          404,
          message || `${this.model.name} not found.`
        );
      return data;
    };
  }
}

module.exports = Base;
