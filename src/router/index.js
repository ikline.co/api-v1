const express = require("express");
const apiRouter = express.Router();

const controllers = require("../controllers");
const BaseController = require("../controllers/Base");
const validations = require("../validations");

const {
  verifyAccessControl,
  verifyAuthentication,
  verifyValidation,
} = require("../middlewares");

/**
 * Gets the name of all the atributes of an object that aren't it's constructor, are a function, and are not private/protected.
 * @param {Object} instance
 */
const getInstanceMethodNames = (instance) => {
  return Object.getOwnPropertyNames(Object.getPrototypeOf(instance)).filter(
    (name) =>
      // 'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them.
      !["constructor", "caller", "calle", "arguments"].includes(name) &&
      typeof instance[name] === "function" &&
      name[0] !== "_"
  );
};

/**
 * API index.
 */
apiRouter.get("/", (req, res, next) => {
  res.send({
    API: {
      name: process.env.API_NAME,
      version: process.env.API_VERSION,
    },
  });
});

/**
 * Iterate over the controllers to register all routes.
 */
const baseController = new BaseController();

Object.keys(controllers).forEach((controllerName) => {
  const controller = controllers[controllerName];
  const controllerRouter = express.Router();

  // Merge target controller methods with base one, and uses Set to ensure that the resulting array has only unique fields.
  const controllerMethodsNames = Array.from(
    new Set([
      ...getInstanceMethodNames(baseController),
      ...getInstanceMethodNames(controller),
    ])
  );

  controllerMethodsNames.forEach((methodName, i, array) => {
    const { path, verb, auth, authz, validate, method } =
      controller[methodName]();

    const middlewares = [];
    if (auth)
      middlewares.push(verifyAuthentication(), verifyAccessControl(authz));

    if (validate) {
      const validation = validations[controllerName][methodName].bind(
        validations[controllerName]
      );

      middlewares.push(...validation(), verifyValidation());
    }

    controllerRouter[verb](path, ...middlewares, method);
  });
  apiRouter.use(controller.path, controllerRouter);
});

module.exports = apiRouter;
