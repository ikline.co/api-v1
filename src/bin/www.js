/**
 * iKline API Entrypoint.
 * @author Gustavo Amaral <gustavoacs99@gmail.com>
 */

const app = require("../app");
const MigrationUtil = require("../utils/MigrationUtil");
const logger = require("../shared/logger");
const port = process.env.PORT || "4000";

/**
 * Starts up the server in the above port.
 */
app.listen(port, () => {
  logger.info(`Server running on port ${port}.`);
  MigrationUtil.runAll();
});
