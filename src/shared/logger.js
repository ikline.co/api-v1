const winston = require("winston");
const fs = require("fs");

if (!fs.existsSync("logs")) {
  fs.mkdirSync("logs");
}

/**
 * Singleton implementation to make certain that there is only one logger instance in the aplication.
 */
const Logger = (() => {
  let instance;

  const createInstance = () => {
    const logger = winston.createLogger({
      level: "info",
      format: winston.format.combine(
        winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.json()
      ),
      transports: [
        new winston.transports.File({
          level: "error",
          filename: "logs/error.log",
          maxsize: 100000,
          maxFiles: 10,
        }),
        new winston.transports.File({
          filename: "logs/combined.log",
          maxsize: 100000,
          maxFiles: 10,
        }),
      ],
    });

    // If we're not in production then **ALSO** log to the `console` with the colorized simple format.
    if (process.env.NODE_ENV !== "production") {
      logger.add(
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
          ),
        })
      );
    }

    return logger;
  };

  return {
    getInstance: () => {
      instance = instance || createInstance();
      return instance;
    },
  };
})();

module.exports = Logger.getInstance();
