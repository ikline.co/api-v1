const shiroTrie = require("shiro-trie");

class AccessControl {
	/**
	 * Verifies if some user has the determinated permissions, based on it's rights and roles. It will return true if the user
	 * has all of the required permissions and roles passed as parameter. If one of the requirements is ommited, only the other
	 * will be evaluated, but if none is passed, it will return true.
	 *
	 * @typedef {Object<string, any>} User Instance of user model.
	 * @param {User} user The User with rights and roles to evaluate his permissions.
	 * @param {Array<String>} [requiredPermissions] Required rights to access the resource.
	 * @param {Array<String>} [requiredRoles] Required roles to access the resource.
	 */
	static isAllowed(user, requiredPermissions, requiredRoles) {
		const _getName = ({ name }) => name;
		const _getRights = ({ rights }) => (rights ? rights.map(_getName) : []);

		const userAuthz = {
			roles: user.roles.map(_getName),
			rights: shiroTrie.newTrie().add(
				// Removing duplicated permissions since a user can have the same permission of his role
				Array.from(
					new Set([
						...user.roles.map(_getRights).flat(),
						...user.rights.map(_getName),
					])
				)
			),
		};

		// Or there is no required permission or user has all the required permissions
		const hasPermission =
			!requiredPermissions ||
			requiredPermissions.every(userAuthz.rights.check.bind(userAuthz.rights));
		// Or there is no required roles or user has all the required roles
		const hasRole =
			!requiredRoles ||
			requiredRoles.every(userAuthz.roles.includes.bind(userAuthz.roles));

		return hasPermission && hasRole;
	}
}

module.exports = AccessControl;
