const db = require("../models");

class Includes {
  rightsAndRoles(companyId) {
    return [
      {
        model: db.right,
        through: {
          attributes: [],
          where: { companyId },
        },
      },
      {
        model: db.role,
        through: { attributes: [] },
        where: { companyId },
        include: [
          {
            model: db.right,
            through: { attributes: [] },
          },
        ],
      },
    ];
  }
}

module.exports = new Includes();
