const PdfMake = require("pdfmake");
const fs = require("fs");

class PdfGenerator {
	/**
	 * Creates an instance of a service that handles the creation of PDF files.
	 * @constructor
	 * @param {Object} [fonts]
	 */
	constructor(fonts) {
		const default_fonts = {
			Roboto: {
				normal: "src/shared/fonts/Roboto-Regular.ttf",
				bold: "src/shared/fonts/Roboto-Regular.ttf",
				italics: "src/shared/fonts/Roboto-Italic.ttf",
				bolditalics: "src/shared/fonts/Roboto-BoldItalic.ttf",
			},
		};
		this.pdfMake = new PdfMake(fonts || default_fonts);
	}

	/**
	 * This method generates a PDF file in the provided path.
	 * @param {String} filePath
	 * @param {Object} pdfDefinition
	 * @param {Object} options
	 */
	async generate(filePath, pdfDefinition, options) {
		return new Promise((resolve) => {
			const pdfDoc = this.pdfMake.createPdfKitDocument(pdfDefinition, options);
			const fileStream = fs.createWriteStream(filePath);
			pdfDoc.pipe(fileStream);
			pdfDoc.end();

			fileStream.on("finish", () => resolve(filePath));
		});
	}
}

module.exports = PdfGenerator;
