"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Company extends Model {
    static associate(models) {
      Company.belongsToMany(models.user, {
        through: "usersCompanies",
        onDelete: "cascade",
      });

      Company.hasMany(models.user);

      Company.hasMany(models.role);

      Company.hasMany(models.userRight);

      Company.hasMany(models.appointment);
    }
  }

  Company.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      details: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      picture: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      openingHours: {
        type: DataTypes.TEXT,
        allowNull: false,
        set(value) {
          if (value) this.setDataValue("openingHours", JSON.stringify(value));
        },
        get() {
          const rawValue = this.getDataValue("openingHours");
          return rawValue ? JSON.parse(rawValue) : rawValue;
        },
      },
      coordinates: {
        type: DataTypes.TEXT,
        allowNull: true,
        set(value) {
          if (value) this.setDataValue("coordinates", JSON.stringify(value));
        },
        get() {
          const rawValue = this.getDataValue("coordinates");
          return rawValue ? JSON.parse(rawValue) : rawValue;
        },
      },
    },
    {
      paranoid: false,
      sequelize,
      modelName: "company",
      tablename: "companies",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );

  return Company;
};
