"use strict";

const { Model } = require("sequelize");
const CryptoUtil = require("../utils/CryptoUtil");
const AuthUtil = require("../utils/AuthUtil");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Verifies if the provided password matches the user one.
     * @param {String} plainPassword The password to be tested.
     * @returns {Boolean} If the password is valid.
     */
    verifyPassword(plainPassword) {
      return CryptoUtil.checkPassword(plainPassword, this.password);
    }

    /**
     * Returns an object where it's only fields are the ones of the aplication token payload.
     * @returns {Object} The payload object.
     */
    toPayload() {
      return {
        id: this.id,
        currentCompanyId: this.currentCompanyId,
      };
    }

    /**
     * Generates and returns a new user token.
     * @returns {String} The generated token.
     */
    generateToken() {
      return AuthUtil.generateToken(this.toPayload());
    }

    /**
     * Generates and injects in the current instance a new refresh token.
     */
    generateRefreshToken() {
      this.refreshToken = AuthUtil.generateRefreshToken(this.toPayload());
    }

    static associate(models) {
      User.belongsTo(models.company, {
        foreignKey: {
          name: "currentCompanyId",
          allowNull: false,
        },
      });

      User.belongsToMany(models.right, {
        through: "usersRights",
        onDelete: "cascade",
      });

      User.belongsToMany(models.role, {
        through: "usersRoles",
        onDelete: "cascade",
      });

      User.belongsToMany(models.company, {
        through: "usersCompanies",
        onDelete: "cascade",
      });
    }
  }

  User.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      refreshToken: {
        type: DataTypes.TEXT,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      plainPassword: {
        type: DataTypes.VIRTUAL,
        set: function (plainPassword) {
          this.setDataValue("plainPassword", plainPassword);
          this.setDataValue("password", CryptoUtil.hashPassword(plainPassword));
        },
        validate: {
          // Add more validations
          isLongEnough: function (plainPassword) {
            if (plainPassword.length < 6) {
              throw new Error("Password validation failed.");
            }
          },
        },
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "user",
      tablename: "users",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt", "password"],
        },
      },
    }
  );

  return User;
};
