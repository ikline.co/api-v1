"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
    static associate(models) {
      Role.belongsToMany(models.user, {
        through: "usersRoles",
        onDelete: "cascade",
      });

      Role.belongsToMany(models.right, {
        through: "rolesRights",
        onDelete: "cascade",
      });

      Role.belongsTo(models.company, {
        foreignKey: {
          allowNull: false,
        },
      });
    }
  }

  Role.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "role",
      tablename: "roles",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
      indexes: [
        {
          unique: true,
          fields: ["label", "companyId"],
        },
      ],
    }
  );

  return Role;
};
