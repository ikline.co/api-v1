"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class UserRight extends Model {
    static associate(models) {
      UserRight.belongsTo(models.company, {
        primaryKey: true,
        allowNull: false,
        onDelete: "cascade",
      });
    }
  }
  UserRight.init(
    {
      rightId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "rights",
          key: "id",
        },
        onDelete: "cascade",
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "users",
          key: "id",
        },
        onDelete: "cascade",
      },
    },
    {
      sequelize,
      modelName: "userRight",
      tablename: "usersRights",
    }
  );

  return UserRight;
};
