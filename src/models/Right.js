"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Right extends Model {
    static associate(models) {
      Right.belongsToMany(models.user, {
        through: "usersRights",
        onDelete: "cascade",
      });

      Right.belongsToMany(models.role, {
        through: "rolesRights",
        onDelete: "cascade",
      });
    }
  }

  Right.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      label: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: "right",
      tablename: "rights",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );

  return Right;
};
