"use strict";

const openingHoursDefault = [
  {
    name: "Domingo",
    day: 0,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Segunda-Feira",
    day: 1,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Terça-Feira",
    day: 2,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Quarta-Feira",
    day: 3,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Quinta-Feira",
    day: 4,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Sexta-Feira",
    day: 5,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
  {
    name: "Sábado",
    day: 6,
    openingHours: [{ open: "Atendimento 24 horas", close: "" }],
  },
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "companies",
      [
        {
          id: 1,
          name: "Hospital Barra D'Or",
          details:
            "O Hospital Barra D’Or reúne tecnologia e equipe multidisciplinar integrada para proporcionar atendimento de qualidade, com foco em segurança para os pacientes que procuram o serviço hospitalar.",
          address:
            " Av. Ayrton Senna, 3079 - Barra da Tijuca, Rio de Janeiro - RJ, 22775-002",
          picture:
            "https://www.google.com/maps/uv?pb=!1s0x9bda3293b16097:0x138a6460a2555cd6!3m1!7e115!4shttps://lh5.googleusercontent.com/p/AF1QipP8X7_VlZPej3alzSVgmk4QETW_0vZfKBMD7LD0%3Dw270-h180-k-no!5sbarra+dor+-+Pesquisa+Google!15zQ2dJZ0FRPT0&imagekey=!1e10!2sAF1QipP8X7_VlZPej3alzSVgmk4QETW_0vZfKBMD7LD0&hl=pt-BR&sa=X&ved=2ahUKEwjhkrP55rTwAhWsq5UCHf5hB7IQoiowCnoECBUQAw",
          openingHours: JSON.stringify(openingHoursDefault),
          coordinates: JSON.stringify({
            lat: -22.983210004614747,
            lon: -43.36704791775823,
          }),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        //
        {
          id: 2,
          name: "Casa de Saúde São José",
          details:
            "A Casa de Saúde São José é hospital geral privado localizado na cidade do Rio de Janeiro, RJ, Brasil. Foi fundado em 1923 e faz parte da Associação Congregação de Santa Catarina, entidade filantrópica que conta com mais de 20 casas em sete estados.",
          address:
            "R. Macedo Sobrinho, 21 - Humaitá, Rio de Janeiro - RJ, 22271-080",
          picture:
            "https://m.extra.globo.com/incoming/6596913-7a9-388/w488h275-PROP/casadesaudesaojose.jpg",
          openingHours: JSON.stringify(openingHoursDefault),
          coordinates: JSON.stringify({
            lat: -22.957838920637325,
            lon: -43.19826638892222,
          }),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("companies", null, {});
  },
};
