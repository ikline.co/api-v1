"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.createTable(
        "companies",
        {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
          },
          name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
          },
          details: {
            type: Sequelize.TEXT,
            allowNull: false,
          },
          address: {
            type: Sequelize.TEXT,
            allowNull: true,
          },
          picture: {
            type: Sequelize.TEXT,
            allowNull: true,
          },
          openingHours: {
            type: Sequelize.TEXT,
            allowNull: false,
          },
          coordinates: {
            type: Sequelize.TEXT,
            allowNull: true,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "rights",
        {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
          },
          name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
          },
          label: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "roles",
        {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
          },
          label: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          companyId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: "companies",
              key: "id",
            },
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "users",
        {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
          },
          name: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          email: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
          },
          refreshToken: {
            type: Sequelize.TEXT,
          },
          password: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          currentCompanyId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: "companies",
              key: "id",
            },
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          deletedAt: {
            allowNull: true,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "usersRights",
        {
          rightId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "rights",
              key: "id",
            },
            onDelete: "cascade",
          },
          userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "users",
              key: "id",
            },
            onDelete: "cascade",
          },
          companyId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "companies",
              key: "id",
            },
            onDelete: "cascade",
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "usersRoles",
        {
          roleId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "roles",
              key: "id",
            },
            onDelete: "cascade",
          },
          userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "users",
              key: "id",
            },
            onDelete: "cascade",
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.createTable(
        "usersCompanies",
        {
          companyId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "companies",
              key: "id",
            },
            onDelete: "cascade",
          },
          userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "users",
              key: "id",
            },
            onDelete: "cascade",
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      return queryInterface.createTable(
        "rolesRights",
        {
          roleId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "roles",
              key: "id",
            },
            onDelete: "cascade",
          },
          rightId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: "rights",
              key: "id",
            },
            onDelete: "cascade",
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.dropTable("rolesRights", { transaction });
      await queryInterface.dropTable("usersRoles", { transaction });
      await queryInterface.dropTable("usersRights", { transaction });
      await queryInterface.dropTable("usersCompanies", { transaction });
      await queryInterface.dropTable("companies", { transaction });
      await queryInterface.dropTable("rights", { transaction });
      return queryInterface.dropTable("roles", { transaction });
    });
  },
};
