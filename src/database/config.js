const baseConfig = {
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_DATABASE,
  host: process.env.DATABASE_HOST,
  dialect: process.env.DATABASE_DIALECT,
  port: process.env.DATABASE_PORT,
  logging: process.env.DATABASE_LOGGING === "true",
  // This configs are shared between enviroments, so they don't need to be env variables.
  define: {
    timestamps: true,
  },
  seederStorage: "sequelize"
};

/**
 * Merge a target configuration object with the base one defined above, addind the base object in each one of the enviroment configs.
 * @param {Object} envConfigs Object with specific configurations to each enviroment.
 */
const mergeConfig = (envConfigs) => {
  return Object.keys(envConfigs).reduce(
    (mergedConfigs, env) => ({
      ...mergedConfigs,
      [env]: { ...baseConfig, ...envConfigs[env] },
    }),
    {}
  );
};

const addionalConfigs = {
  test: {},
  development: {},
  production: {
    pool: {
      max: 20,
      min: 5,
      acquire: 30000,
      idle: 10000,
    },
  },
};

module.exports = mergeConfig(addionalConfigs);
