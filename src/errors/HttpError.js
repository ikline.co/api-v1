class HttpError extends Error {
  /**
   * Creates an instance of an error with a status atribute making reference to a http status code.
   * @param {Number} [status] The http code making reference to a http status code, default value is 500.
   * @param {String} [message] The message of the error, default value is an empty string.
   */
  constructor(status, message) {
    super(message);
    this.status = status || 500;
  }
}

module.exports = HttpError;
