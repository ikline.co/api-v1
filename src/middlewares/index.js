const ImportUtil = require("../utils/ImportUtil");

module.exports = ImportUtil.getDirImports(__dirname, [__filename]);
