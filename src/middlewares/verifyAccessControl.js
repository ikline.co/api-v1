const db = require("../models");
const includes = require("../shared/includes");
const AccessControl = require("../shared/AccessControl");

/**
 * Verifies if the authenticated user has enought rights to access some resource.
 * @param {*} param0
 */
const verifyAccessControl = ({ requiredPermissions, requiredRoles }) => {
  return (req, res, next) => {
    // First lets check if there is an user under the session:
    if (!req.user) {
      let err = new Error("User is not authenticated.");
      err.status = 401;
      return next(err);
    }

    return db.user
      .findByPk(req.user.id, {
        include: includes.rightsAndRoles(req.user.currentCompanyId),
      })
      .getUserWithRightsAndRolesById(req.user.id)
      .then((user) => {
        if (!user) {
          const err = new Error("Authenticated user was not found.");
          err.status = 410;
          return next(err);
        }
        return user;
      })
      .then((user) => {
        if (AccessControl.isAllowed(user, requiredPermissions, requiredRoles))
          return next();

        const err = new Error(
          "Forbiden for lacking any of these permissions: " +
            requiredPermissions.join(", ")
        );
        err.status = 403;
        return next(err);
      })
      .catch(next);
  };
};

module.exports = verifyAccessControl;
