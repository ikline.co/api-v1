const { HttpError } = require("../errors");

const AuthUtil = require("../utils/AuthUtil");

/**
 * Verifies if the request origin is authenticated, if not, throws an error. If the authorization is valid, adds a new identification field in the request.
 */
const verifyAuthentication = () => {
  return (req, res, next) => {
    const bearerHeader = req.headers["authorization"];
    if (!bearerHeader)
      return next(new HttpError(401, "Request without access token."));

    const token = bearerHeader.split(" ").pop();

    return AuthUtil.verifyJWT(token)
      .then((decoded) => {
        req.user = decoded;
        next();
      })
      .catch((error) => {
        next(
          new HttpError(
            401,
            `Access token is invalid. ${
              error.expiredAt ? `Expired at ${error.expiredAt}.` : ""
            }`
          )
        );
      });
  };
};

module.exports = verifyAuthentication;
