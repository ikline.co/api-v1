const ExpressValidator = require("./ExpressValidator");
const { param, query } = require("express-validator");

class Base {
  validator = ExpressValidator;

  delete() {
    return [this.validator.id(param)];
  }

  find() {
    return [this.validator.id(param)];
  }

  list() {
    return [
      this.validator.nonNegativeInteger(query, "page", { required: false }),
      this.validator.positiveInteger(query, "size", { required: false }),
    ];
  }

  update() {
    return [];
  }

  create() {
    return [];
  }
}

module.exports = Base;
