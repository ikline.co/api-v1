const Base = require("./Base");

const { body, param } = require("express-validator");

class Appointment extends Base {
  constructor() {
    super();
  }

  create() {
    return [
      this.validator.id(body, { fieldName: "companyId" }),
      this.validator.date(body, "date"),
    ];
  }

  update() {
    return [this.validator.id(param), this.validator.date(body, "date")];
  }
}

module.exports = new Appointment();
