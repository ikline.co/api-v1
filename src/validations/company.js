const Base = require("./Base");

const { body } = require("express-validator");

class Company extends Base {
  constructor() {
    super();
  }
  create() {
    return [this.validator.string(body, "name")];
  }

  update() {
    return [this.validator.string(body, "name", { required: false })];
  }
}

module.exports = new Company();
