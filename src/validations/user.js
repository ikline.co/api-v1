const Base = require("./Base");

const { body, param } = require("express-validator");

class User extends Base {
  constructor() {
    super();
  }

  changeCurrentCompany() {
    return [this.validator.id(body, { fieldName: "companyId" })];
  }

  forgot() {
    return [this.validator.email(body)];
  }

  login() {
    return [this.validator.email(body), this.validator.password(body)];
  }

  refreshToken() {
    return [this.validator.refreshToken(body)];
  }

  reset() {
    return [this.validator.refreshToken(body), this.validator.password(body)];
  }

  signUp() {
    return [
      this.validator.array(body, "rights"),
      this.validator.array(body, "roles"),
      this.validator.string(body, "name"),
      this.validator.id(body, { fieldName: "companyId" }),
      this.validator.password(body),
      this.validator.email(body),
    ];
  }

  update() {
    return [
      this.validator.email(body, { required: false }),
      this.validator.array(body, "roles", { required: false }),
      this.validator.array(body, "rights", { required: false }),
      this.validator.string(body, "name", { required: false }),
      this.validator.id(param),
    ];
  }
}

module.exports = new User();
