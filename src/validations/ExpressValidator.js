const CPF = require("cpf");

// It still needs to be integrated with i18n.
class ExpressValidator {
  /**
   * Validates a password field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the password, by default it's value is "password".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static password(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "password",
      required: true,
      ...options,
    };
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .isLength({ min: 6, max: 30 })
      .withMessage(`The field ${fieldName} must have length between 6 and 30.`);
    // .bail()
    // .isStrongPassword({
    // 	minLowercase: 1,
    // 	minUppercase: 1,
    // 	minNumbers: 1,
    // })
    // .withMessage(
    // 	`The field ${fieldName} must have at least one number, one lowerCase and one uppercase character.`
    // );
  }

  /**
   * Validates an email field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the email, by default it's value is "email".
   * @param {Boolean} [options.required]  If the field is required, default is true.
   */
  static email(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "email",
      required: true,
      ...options,
    };

    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .isEmail()
      .withMessage(`The field ${fieldName} must be a valid email.`);
    // Emails must be normalized, but since we weren't doing it before, it may be a problem
    // .normalizeEmail(),
  }

  /**
   * Validates refreshToken field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the refreshToken, by default it's value is "refreshToken".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static refreshToken(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "refreshToken",
      required: true,
      ...options,
    };
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .isJWT()
      .withMessage(`The field ${fieldName} must be a valid JWT token.`);
    // .bail();
    // .custom((token, { req }) => "Verify token here")
    // .withMessage(`The token in ${fieldName} is not valid.`)
  }

  /**
   * Validates a slug field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the slug, by default it's value is "slug".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static slug(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "slug",
      required: true,
      ...options,
    };
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .isSlug()
      .withMessage(`The field ${fieldName} must be a slug.`);
  }

  /**
   * Validates a url field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation
   * @param {String} [options.fieldName] The name of the field with the url, by default it's value is "url".
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {Boolean} [options.allowEmpty] If empty strings are allowed, default is false.
   * @param {Boolean} [options.require_protocol] If set as true isURL will return false if protocol is not present in the URL, default is false.
   * @param {Boolean} [options.require_valid_protocol] If set as true will check if the URL's protocol is present in the protocols option, default is true.
   * @param {Array<String>} [options.protocols] Valid protocols can be modified with this option, default is ['http','https','ftp'].
   * @param {Boolean} [options.require_host] If set as false isURL will not check if host is present in the URL, default is true.
   * @param {Boolean} [options.require_port] If set as true isURL will check if port is present in the URL, default is false.
   * @param {Boolean} [options.allow_protocol_relative_urls] If set as true protocol relative URLs will be allowed, default is false.
   * @param {Boolean} [options.validate_length] If set as false isURL will skip string length validation (2083 characters is IE max URL length), default is true.
   */
  static url(validator, options = {}) {
    const { fieldName, required, allowEmpty, ...urlOptions } = {
      fieldName: "url",
      required: true,
      require_tld: process.env.NODE_ENV !== "development",
      ...options,
    };
    return ExpressValidator.string(validator, fieldName, {
      required,
      allowEmpty,
    })
      .bail()
      .isURL(urlOptions)
      .withMessage(`The field ${fieldName} must be a valid URL.`);
  }

  /**
   * Validates a hexadecimal color field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the hexadecimal color, by default it's value is "color".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static hexColor(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "color",
      required: true,
      ...options,
    };
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .isHexColor()
      .withMessage(`The field ${fieldName} must be a hexadecimal color.`);
  }

  /**
   * Validates a period string field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the period string, by default it's value is "period".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static period(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "period",
      required: true,
      ...options,
    };
    const isPeriod = (string) => /^0*[1-9]\d*[M,Y]$/.test(string);
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .custom(isPeriod)
      .withMessage(
        `The field ${fieldName} must be a string period in format *M or *Y, where * indicates any positive number.`
      );
  }

  /**
   * Validates a string field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {Boolean} [options.allowEmpty] If empty strings are allowed, default is false.
   */
  static string(validator, fieldName, options = {}) {
    const { required, allowEmpty } = {
      required: true,
      allowEmpty: false,
      ...options,
    };

    const validation = (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isString()
      .withMessage(`The field ${fieldName} must be a string.`);

    return allowEmpty
      ? validation
      : validation
          .bail()
          .notEmpty()
          .withMessage(
            `The field ${(fieldName, allowEmpty)} cannot be an empty string.`
          );
  }

  /**
   * Validates a boolean field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static boolean(validator, fieldName, options = {}) {
    const { required } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isBoolean()
      .withMessage(`The field ${fieldName} must be a boolean.`);
  }

  /**
   * Validates a JSON field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {Boolean} [options.allow_primitives] If true, the primitives 'true', 'false' and 'null' are accepted as valid JSON values, default is false.
   */
  static JSON(validator, fieldName, options = {}) {
    const { required, allow_primitives } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isJSON({ allow_primitives })
      .withMessage(`The field ${fieldName} must be a JSON.`);
  }

  /**
   * Validates an array field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {Number} [options.min] Minimum array length.
   * @param {Number} [options.max] Maximum array length.
   */
  static array(validator, fieldName, options = {}) {
    const { required, min, max } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isArray({ min, max })
      .withMessage(
        `The field ${fieldName} must be an array with length inside [${
          min || "*"
        },${max || "*"}], where * symbolizes any number.`
      );
  }

  /**
   * Validates an object field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {Boolean} [options.strict] If set to false the validation passes also for object and null types, defaults to true.
   */
  static object(validator, fieldName, options) {
    const { required, strict } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isObject({ strict })
      .withMessage(`The field ${fieldName} must be an object.`);
  }

  /**
   * Validates an id field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {String} [options.fieldName] The name of the field with the id, by default it's value is "id".
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static id(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "id",
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isInt({ min: 1 })
      .withMessage(`The field ${fieldName} must be a positive integer.`);
  }

  /**
   * Validates a non negative integer field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static nonNegativeInteger(validator, fieldName, options = {}) {
    const { required } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isInt({ min: 0 })
      .withMessage(`The field ${fieldName} must be a non negative integer.`);
  }

  /**
   * Validates an positive integer field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   */
  static positiveInteger(validator, fieldName, options = {}) {
    const { required } = {
      required: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isInt({ min: 1 })
      .withMessage(`The field ${fieldName} must be a positive integer.`);
  }

  /**
   * Validates a date field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   * @param {Boolean} [options.required] If the field is required, default is true.
   * @param {String} [options.format] Date format and defaults to YYYY/MM/DD
   * @param {Boolean} [options.strictMode] If set to true, the validator will reject inputs different from format, default is true.
   * @param {Array} [options.delimiters] An array of allowed date delimiters and defaults to ['/', '-'].
   */
  static date(validator, fieldName, options = {}) {
    const { required, format, strictMode, delimiters } = {
      required: true,
      strictMode: true,
      ...options,
    };
    return (
      required
        ? ExpressValidator.required(validator, fieldName).bail()
        : validator(fieldName)
    )
      .if(validator(fieldName).exists({ checkNull: true }))
      .isDate({ format, strictMode, delimiters })
      .withMessage(
        `The field ${fieldName} must be a date on format ${
          format || "YYYY/MM/DD"
        }${
          strictMode
            ? "."
            : `, with ${
                (delimiters && delimiters.join(" or ")) || "/ or -"
              } as delimiters.`
        }`
      );
  }

  /**
   * Validates a required field using express-chain API.
   * @param {*} validator The validator to be used in the chain, i.e "check", "body" etc.
   * @param {String} fieldName The name of the field to be validated.
   * @param {Object} [options] An object with optional parameters to customize the validation.
   */
  static required(validator, fieldName, options = {}) {
    return validator(fieldName)
      .exists({ checkNull: true })
      .withMessage(`The field ${fieldName} is required.`);
  }

  static cpf(validator, options = {}) {
    const { fieldName, required } = {
      fieldName: "cpf",
      required: true,
      ...options,
    };
    const isValid = (value) => {
      return CPF.isValid(CPF.format(value));
    };
    return ExpressValidator.string(validator, fieldName, { required })
      .bail()
      .custom(isValid)
      .withMessage(`The field ${fieldName} must be a valid cpf string.`);
  }
}

module.exports = ExpressValidator;
