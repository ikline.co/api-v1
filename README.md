<div style="text-align:center"><img style="object-fit:cover;"src="./assets/images/logo.png" /></div>

<br/>
<br/>
<br/>

## iKline API

###### Application for scheduling medical appointments online.

Backend of both projects of [iKline.co](https://gitlab.com/ikline.co), iKline Mobile and iKline Enterprise, the iKline API is a [Node.js](https://github.com/nodejs/node) based API, with [Express.js](https://github.com/expressjs/express).

The infrastructure also relies on Sequelize ORM, to manage a postreSQL database.

Currently we only support EN language, but support in PT should be released soon.

<br/>

## Getting Started

The next few steps shows how to setup your enviroment and startup your server.

---

##### 1. Build docker-compose containers.

```sh
> docker-compose up
```

---

##### 2. Run seeds to populate the development database.

```sh
> yarn populate:db
```

---

##### 3. Your server is all setup and running on <code>port 4000</code> 🚀 .

---
